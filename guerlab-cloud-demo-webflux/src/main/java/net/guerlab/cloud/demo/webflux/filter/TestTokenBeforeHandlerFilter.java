/*
 * Copyright 2018-2021 guerlab.net and other contributors.
 *
 * Licensed under the GNU LESSER GENERAL PUBLIC LICENSE, Version 3 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.gnu.org/licenses/lgpl-3.0.html
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.guerlab.cloud.demo.webflux.filter;

import net.guerlab.cloud.auth.webflux.filter.AbstractTokenBeforeHandlerFilter;
import net.guerlab.cloud.commons.ip.IpUtils;
import net.guerlab.cloud.test.core.context.TestContextHandler;
import net.guerlab.cloud.test.core.domain.TestUserInfo;
import net.guerlab.cloud.test.core.factory.TestJwtTokenFactory;
import net.guerlab.cloud.test.core.properties.TestAuthWebProperties;
import net.guerlab.cloud.web.core.properties.ResponseAdvisorProperties;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.web.reactive.result.method.annotation.RequestMappingHandlerMapping;

/**
 * 测试用token前置处理
 *
 * @author guer
 */
public class TestTokenBeforeHandlerFilter extends AbstractTokenBeforeHandlerFilter<TestAuthWebProperties> {

    private final TestJwtTokenFactory tokenFactory;

    public TestTokenBeforeHandlerFilter(ResponseAdvisorProperties responseAdvisorProperties,
            RequestMappingHandlerMapping requestMappingHandlerMapping, TestAuthWebProperties authProperties,
            TestJwtTokenFactory tokenFactory) {
        super(responseAdvisorProperties, requestMappingHandlerMapping, authProperties);
        this.tokenFactory = tokenFactory;
    }

    @Override
    protected boolean accept(String token, ServerHttpRequest request) {
        String ip = IpUtils.getIp(request);

        return tokenFactory.enabled() && tokenFactory.acceptAccessToken(token) && tokenFactory.acceptIp(ip);
    }

    @Override
    protected void setTokenInfo(String token) {
        TestUserInfo infoFromToken = tokenFactory.parseByAccessToken(token);

        TestContextHandler.setName(infoFromToken.getName());
    }
}
