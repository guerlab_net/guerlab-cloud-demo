/*
 * Copyright 2018-2021 guerlab.net and other contributors.
 *
 * Licensed under the GNU LESSER GENERAL PUBLIC LICENSE, Version 3 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.gnu.org/licenses/lgpl-3.0.html
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.guerlab.cloud.demo.webflux.autoconfigure;

import net.guerlab.cloud.demo.webflux.filter.TestTokenAfterHandlerFilter;
import net.guerlab.cloud.demo.webflux.filter.TestTokenBeforeHandlerFilter;
import net.guerlab.cloud.test.core.factory.TestJwtTokenFactory;
import net.guerlab.cloud.test.core.properties.TestAuthWebProperties;
import net.guerlab.cloud.web.core.properties.ResponseAdvisorProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.result.method.annotation.RequestMappingHandlerMapping;

/**
 * 测试用授权过滤器配置
 *
 * @author guer
 */
@Configuration
public class TestAuthFiltersAutoconfigure {

    @Bean
    public TestTokenBeforeHandlerFilter testTokenHandlerFilter(ResponseAdvisorProperties responseAdvisorProperties,
            RequestMappingHandlerMapping requestMappingHandlerMapping, TestAuthWebProperties authProperties,
            TestJwtTokenFactory tokenFactory) {
        return new TestTokenBeforeHandlerFilter(responseAdvisorProperties, requestMappingHandlerMapping, authProperties,
                tokenFactory);
    }

    @Bean
    public TestTokenAfterHandlerFilter testTokenAfterHandlerFilter(ResponseAdvisorProperties responseAdvisorProperties,
            RequestMappingHandlerMapping requestMappingHandlerMapping, TestAuthWebProperties authProperties) {
        return new TestTokenAfterHandlerFilter(responseAdvisorProperties, requestMappingHandlerMapping, authProperties);
    }
}
