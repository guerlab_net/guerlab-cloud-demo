/*
 * Copyright 2018-2021 guerlab.net and other contributors.
 *
 * Licensed under the GNU LESSER GENERAL PUBLIC LICENSE, Version 3 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.gnu.org/licenses/lgpl-3.0.html
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.guerlab.cloud.demo.webflux.filter;

import net.guerlab.cloud.auth.webflux.filter.AbstractTokenAfterHandlerFilter;
import net.guerlab.cloud.commons.exception.AccessTokenInvalidException;
import net.guerlab.cloud.test.core.context.TestContextHandler;
import net.guerlab.cloud.test.core.properties.TestAuthWebProperties;
import net.guerlab.cloud.web.core.properties.ResponseAdvisorProperties;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.reactive.result.method.annotation.RequestMappingHandlerMapping;

/**
 * 测试用token后置处理
 *
 * @author guer
 */
public class TestTokenAfterHandlerFilter extends AbstractTokenAfterHandlerFilter<TestAuthWebProperties> {

    public TestTokenAfterHandlerFilter(ResponseAdvisorProperties responseAdvisorProperties,
            RequestMappingHandlerMapping requestMappingHandlerMapping, TestAuthWebProperties authProperties) {
        super(responseAdvisorProperties, requestMappingHandlerMapping, authProperties);
    }

    @Override
    protected void preHandleWithoutToken() {
        if (StringUtils.isBlank(TestContextHandler.getName())) {
            throw new AccessTokenInvalidException();
        }
    }
}
