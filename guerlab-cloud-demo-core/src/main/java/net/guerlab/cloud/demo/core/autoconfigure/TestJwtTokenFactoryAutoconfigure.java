/*
 * Copyright 2018-2021 guerlab.net and other contributors.
 *
 * Licensed under the GNU LESSER GENERAL PUBLIC LICENSE, Version 3 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.gnu.org/licenses/lgpl-3.0.html
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.guerlab.cloud.demo.core.autoconfigure;

import net.guerlab.cloud.demo.core.factory.TestJwtTokenFactory;
import net.guerlab.cloud.demo.core.properties.TestAuthWebProperties;
import net.guerlab.cloud.demo.core.properties.TestJwtTokenFactoryProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 测试jwt token 工厂自动配置
 *
 * @author guer
 */
@Configuration
@EnableConfigurationProperties({ TestJwtTokenFactoryProperties.class, TestAuthWebProperties.class })
public class TestJwtTokenFactoryAutoconfigure {

    @Bean
    public TestJwtTokenFactory testJwtTokenFactory(TestJwtTokenFactoryProperties properties) {
        TestJwtTokenFactory factory = new TestJwtTokenFactory();
        factory.setProperties(properties);
        return factory;
    }
}
