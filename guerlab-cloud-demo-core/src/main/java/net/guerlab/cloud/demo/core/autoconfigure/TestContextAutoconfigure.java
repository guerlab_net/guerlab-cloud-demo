/*
 * Copyright 2018-2021 guerlab.net and other contributors.
 *
 * Licensed under the GNU LESSER GENERAL PUBLIC LICENSE, Version 3 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.gnu.org/licenses/lgpl-3.0.html
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.guerlab.cloud.demo.core.autoconfigure;

import net.guerlab.cloud.demo.core.context.TestContextHandler;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 测试用上下文处理器自动配置
 *
 * @author guer
 */
@Configuration
public class TestContextAutoconfigure {

    /**
     * 构造测试用上下文处理器
     *
     * @return 测试用上下文处理器
     */
    @Bean
    public TestContextHandler testContextHandler() {
        return new TestContextHandler();
    }
}
