/*
 * Copyright 2018-2021 guerlab.net and other contributors.
 *
 * Licensed under the GNU LESSER GENERAL PUBLIC LICENSE, Version 3 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.gnu.org/licenses/lgpl-3.0.html
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.guerlab.cloud.demo.core.properties;

import net.guerlab.cloud.auth.properties.JwtTokenFactoryProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * 测试jwt token 工厂配置
 *
 * @author guer
 */
@ConfigurationProperties(prefix = TestJwtTokenFactoryProperties.PREFIX)
public class TestJwtTokenFactoryProperties extends JwtTokenFactoryProperties {

    public static final String PREFIX = "auth.test.token-factory.jwt";
}
