/*
 * Copyright 2018-2021 guerlab.net and other contributors.
 *
 * Licensed under the GNU LESSER GENERAL PUBLIC LICENSE, Version 3 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.gnu.org/licenses/lgpl-3.0.html
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.guerlab.cloud.demo.webmvc.interceptor;

import net.guerlab.cloud.auth.webmvc.interceptor.AbstractHandlerInterceptor;
import net.guerlab.cloud.commons.exception.AccessTokenInvalidException;
import net.guerlab.cloud.test.core.context.TestContextHandler;
import org.apache.commons.lang3.StringUtils;

/**
 * 测试用token后置处理
 *
 * @author guer
 */
public class TestTokenHandlerAfterInterceptor extends AbstractHandlerInterceptor {

    @Override
    protected void preHandleWithoutToken() {
        if (StringUtils.isBlank(TestContextHandler.getName())) {
            throw new AccessTokenInvalidException();
        }
    }
}
