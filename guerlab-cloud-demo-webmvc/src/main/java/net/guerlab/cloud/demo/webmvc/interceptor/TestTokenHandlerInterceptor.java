/*
 * Copyright 2018-2021 guerlab.net and other contributors.
 *
 * Licensed under the GNU LESSER GENERAL PUBLIC LICENSE, Version 3 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.gnu.org/licenses/lgpl-3.0.html
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.guerlab.cloud.demo.webmvc.interceptor;

import net.guerlab.cloud.auth.webmvc.interceptor.AbstractTokenHandlerInterceptor;
import net.guerlab.cloud.commons.ip.IpUtils;
import net.guerlab.cloud.test.core.context.TestContextHandler;
import net.guerlab.cloud.test.core.domain.TestUserInfo;
import net.guerlab.cloud.test.core.factory.TestJwtTokenFactory;
import net.guerlab.cloud.test.core.properties.TestAuthWebProperties;

import javax.servlet.http.HttpServletRequest;

/**
 * 测试用token处理
 *
 * @author guer
 */
public class TestTokenHandlerInterceptor extends AbstractTokenHandlerInterceptor<TestAuthWebProperties> {

    private final TestJwtTokenFactory tokenFactory;

    public TestTokenHandlerInterceptor(TestJwtTokenFactory tokenFactory) {
        this.tokenFactory = tokenFactory;
    }

    @Override
    protected boolean accept(String token, HttpServletRequest request) {
        String ip = IpUtils.getIp(request);

        return tokenFactory.enabled() && tokenFactory.acceptAccessToken(token) && tokenFactory.acceptIp(ip);
    }

    @Override
    protected void setTokenInfo(String token) {
        TestUserInfo infoFromToken = tokenFactory.parseByAccessToken(token);

        TestContextHandler.setName(infoFromToken.getName());
    }
}
