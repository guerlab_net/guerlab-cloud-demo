/*
 * Copyright 2018-2021 guerlab.net and other contributors.
 *
 * Licensed under the GNU LESSER GENERAL PUBLIC LICENSE, Version 3 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.gnu.org/licenses/lgpl-3.0.html
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.guerlab.cloud.demo.webmvc.autoconfigure;

import net.guerlab.cloud.auth.webmvc.autoconfig.AbstractAuthInterceptorAutoconfigure;
import net.guerlab.cloud.demo.webmvc.interceptor.TestTokenHandlerAfterInterceptor;
import net.guerlab.cloud.demo.webmvc.interceptor.TestTokenHandlerInterceptor;
import net.guerlab.cloud.test.core.factory.TestJwtTokenFactory;
import net.guerlab.cloud.test.core.properties.TestAuthWebProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;

/**
 * 测试用授权过滤器配置
 *
 * @author guer
 */
@Configuration
public class TestAuthInterceptorAutoconfigure extends AbstractAuthInterceptorAutoconfigure<TestAuthWebProperties> {

    private final TestTokenHandlerAfterInterceptor tokenHandlerAfterInterceptor = new TestTokenHandlerAfterInterceptor();

    public TestAuthInterceptorAutoconfigure(TestAuthWebProperties properties) {
        super(properties);
    }

    @Bean
    public TestTokenHandlerInterceptor testTokenHandlerInterceptor(TestJwtTokenFactory tokenFactory) {
        return new TestTokenHandlerInterceptor(tokenFactory);
    }

    @Bean
    public TestTokenHandlerAfterInterceptor testTokenHandlerAfterInterceptor() {
        return tokenHandlerAfterInterceptor;
    }

    @Override
    protected void addInterceptorsInternal(InterceptorRegistry registry) {
        setPathPatterns(registry.addInterceptor(tokenHandlerAfterInterceptor));
    }
}
